# Laravel 6.X, Mysql  and Bootstrap

Laravel Cricket Schedule, Fixtures and Matches is a simple Cricket Schedule application built on Laravel 6.X, Mysql, and Bootstrap

| Laravel Cricket Schedule, Fixtures and Matches |
| :------------ |
| Built on Laravel 6.x |
| Built on Bootstrap |
| Makes use of MySQL Database |
| Uses Laravel Repositories |
| Uses Laravel Middleware |
| Uses jQuery|

### I. Quick Project Setup - Installation

###### 1. Clone the repository using the following command in terminal:

	`sudo git clone https://gitlab.com/Harish.Mogilipuri/cricket-schedule-application.git`

###### 2. Pull and Install Laravel Dependencies:

	`sudo composer update`

###### 3. Generate Key:

	`php artisan key:generate`

###### 4. Create your MySQL Database.
 
    Database name : cricket  

###### 5. Copy the example file  using the following command in terminal from the projects root folder:

	`sudo cp .env.example .env`

###### 6. Configure the Database settings in the ```.env``` file.

###### Skip Below step import sql file "cricket.sql"

###### 7. Setup Database Schema using the following command in terminal from the projects root folder:

	`sudo php artisan migrate` 
    
   `php artisan migrate:fresh --seed` ( if required use this for refresh migration with seeds )

###### 8. Run below command to genererate (insert) random data in teams , players, fixtures:

	`php artisan db:seed --class=TeamsTableSeeder`
    `php artisan db:seed --class=PlayersTableSeeder`
	`php artisan db:seed --class=FixturesTableSeeder`

###### 8. Run below command to run project:

	`php artisan serve --port=8106`

###### 9. Open below link in broswer:

  Laravel development server started: http://127.0.0.1:8106
  
### II. Middlewares
* [XSS] -> Middleware (This middleware used for Preventing  Script Tag injections in Forms)
* [BasicAuth] -> Middleware (This middleware used for Basic Authentication)

### III. Basic Authentication

User Name : harish

Password : harish

###### Sample Screens:

1. Home: https://gitlab.com/Harish.Mogilipuri/cricket-schedule-application/-/raw/master/public/demo_screens/home.png

2. Matches: https://gitlab.com/Harish.Mogilipuri/cricket-schedule-application/-/raw/master/public/demo_screens/matches.png

3. Players: https://gitlab.com/Harish.Mogilipuri/cricket-schedule-application/-/raw/master/public/demo_screens/players.png

4. Teams: https://gitlab.com/Harish.Mogilipuri/cricket-schedule-application/-/raw/master/public/demo_screens/teams.png

5. Points: https://gitlab.com/Harish.Mogilipuri/cricket-schedule-application/-/raw/master/public/demo_screens/points.png