-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 22, 2020 at 08:49 PM
-- Server version: 5.7.29-0ubuntu0.16.04.1
-- PHP Version: 7.2.29-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cricket`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fixtures`
--

CREATE TABLE `fixtures` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_a` int(11) NOT NULL,
  `team_b` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `venue` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `winner` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active,0-inactive',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'soft_deleted_at',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fixtures`
--

INSERT INTO `fixtures` (`id`, `team_a`, `team_b`, `date`, `venue`, `winner`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2020-03-18 09:00:00', 'Eden Park, Auckland, New Zealand', 1, 1, NULL, NULL, NULL),
(2, 1, 3, '2020-03-19 09:00:00', 'Wanderers, Windhoek, Namibia', 1, 1, NULL, NULL, NULL),
(3, 1, 6, '2020-03-19 09:00:00', 'Wanderers, Windhoek, Namibia', 1, 1, NULL, NULL, NULL),
(4, 4, 5, '2020-03-19 09:00:00', 'National Stadium, Karachi, Pakistan', 4, 1, NULL, NULL, NULL),
(5, 1, 6, '2020-04-01 09:00:00', 'National Stadium, Karachi, Pakistan', 0, 1, NULL, NULL, NULL),
(6, 4, 5, '2020-04-19 09:00:00', 'National Stadium, Karachi, Pakistan', 0, 1, NULL, NULL, NULL),
(7, 3, 5, '2020-02-19 09:00:00', 'National Stadium,  Visakhapatnam, Andhra Pradesh', 3, 1, NULL, NULL, NULL),
(8, 3, 5, '2020-02-20 09:00:00', 'National Stadium, Visakhapatnam, Andhra Pradesh', 3, 1, NULL, NULL, NULL),
(9, 2, 4, '2020-01-20 09:00:00', 'Cricket Stadium,  Visakhapatnam, Andhra Pradesh', 4, 1, NULL, NULL, NULL),
(10, 4, 6, '2020-01-03 09:00:00', ' Cricket Stadium,  Visakhapatnam, Andhra Pradesh', 6, 1, NULL, NULL, NULL),
(11, 1, 2, '2020-03-18 09:00:00', 'Eden Park, Auckland, New Zealand', 1, 1, NULL, NULL, NULL),
(12, 1, 3, '2020-03-19 09:00:00', 'Wanderers, Windhoek, Namibia', 1, 1, NULL, NULL, NULL),
(13, 1, 6, '2020-03-19 09:00:00', 'Wanderers, Windhoek, Namibia', 1, 1, NULL, NULL, NULL),
(14, 4, 5, '2020-03-19 09:00:00', 'National Stadium, Karachi, Pakistan', 4, 1, NULL, NULL, NULL),
(15, 1, 6, '2020-04-01 09:00:00', 'National Stadium, Karachi, Pakistan', 0, 1, NULL, NULL, NULL),
(16, 4, 5, '2020-04-19 09:00:00', 'National Stadium, Karachi, Pakistan', 0, 1, NULL, NULL, NULL),
(17, 3, 5, '2020-02-19 09:00:00', 'National Stadium,  Visakhapatnam, Andhra Pradesh', 3, 1, NULL, NULL, NULL),
(18, 3, 5, '2020-02-20 09:00:00', 'National Stadium, Visakhapatnam, Andhra Pradesh', 3, 1, NULL, NULL, NULL),
(19, 2, 4, '2020-01-20 09:00:00', 'Cricket Stadium,  Visakhapatnam, Andhra Pradesh', 4, 1, NULL, NULL, NULL),
(20, 4, 6, '2020-01-03 09:00:00', ' Cricket Stadium,  Visakhapatnam, Andhra Pradesh', 6, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_03_21_191452_create_teams_table', 1),
(5, '2020_03_21_191531_create_players_table', 1),
(6, '2020_03_21_191545_create_points_table', 1),
(7, '2020_03_21_191559_create_fixtures_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_id` int(11) NOT NULL,
  `player_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_pic` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jersey_no` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `matches` int(11) NOT NULL,
  `runs` int(11) NOT NULL,
  `highest_scores` int(11) NOT NULL,
  `fifties` int(11) NOT NULL,
  `hundreds` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `ranking` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active,0-inactive',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'soft_deleted_at',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `team_id`, `player_slug`, `first_name`, `last_name`, `profile_pic`, `jersey_no`, `country`, `matches`, `runs`, `highest_scores`, `fifties`, `hundreds`, `role`, `ranking`, `description`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 5, 'gianni', 'Gianni', 'Berge', 'https://lorempixel.com/640/480/?34968', 11, 1, 161, 7683, 169, 14, 19, 4, 2, 'Walter Ritchie', 1, NULL, '2020-03-22 09:47:43', '2020-03-22 09:47:43'),
(2, 2, 'makenzie', 'Makenzie', 'Weimann', 'https://lorempixel.com/640/480/?13605', 20, 2, 61, 11112, 161, 23, 14, 3, 2, 'Dr. Taryn Cronin PhD', 1, NULL, '2020-03-22 09:47:43', '2020-03-22 09:47:43'),
(3, 6, 'holly', 'Holly', 'Sawayn', 'https://lorempixel.com/640/480/?95422', 4, 3, 232, 48004, 131, 17, 4, 1, 4, 'Helene Wunsch', 1, NULL, '2020-03-22 09:47:43', '2020-03-22 09:47:43'),
(4, 6, 'jovanny', 'Jovanny', 'Brekke', 'https://lorempixel.com/640/480/?79436', 1, 1, 230, 16252, 113, 11, 20, 1, 1, 'Theodora Johnston V', 1, NULL, '2020-03-22 09:47:43', '2020-03-22 09:47:43'),
(5, 3, 'justus', 'Justus', 'Grimes', 'https://lorempixel.com/640/480/?21390', 13, 1, 215, 36677, 154, 33, 31, 6, 5, 'Hulda Leffler', 1, NULL, '2020-03-22 09:47:43', '2020-03-22 09:47:43'),
(6, 6, 'christa', 'Christa', 'Lockman', 'https://lorempixel.com/640/480/?11074', 19, 1, 149, 20755, 137, 5, 36, 5, 6, 'Marjorie Hyatt', 1, NULL, '2020-03-22 09:47:43', '2020-03-22 09:47:43'),
(7, 6, 'juston', 'Juston', 'Funk', 'https://lorempixel.com/640/480/?63429', 10, 3, 28, 19313, 97, 11, 46, 5, 4, 'Era Littel', 1, NULL, '2020-03-22 09:47:43', '2020-03-22 09:47:43'),
(8, 1, 'madie', 'Madie', 'Krajcik', 'https://lorempixel.com/640/480/?93324', 16, 3, 198, 34857, 178, 42, 46, 2, 2, 'Catharine Hermiston', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(9, 4, 'clifford', 'Clifford', 'Schaefer', 'https://lorempixel.com/640/480/?18570', 17, 6, 1, 42206, 86, 21, 36, 6, 4, 'Prof. Dayana Hammes IV', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(10, 3, 'winfield', 'Winfield', 'Jast', 'https://lorempixel.com/640/480/?72592', 3, 5, 380, 25627, 88, 9, 34, 5, 1, 'Ms. Frances Swaniawski III', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(11, 3, 'ressie', 'Ressie', 'Prohaska', 'https://lorempixel.com/640/480/?57875', 14, 5, 339, 17870, 52, 42, 46, 6, 3, 'Prof. Consuelo Reynolds DVM', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(12, 1, 'jaylin', 'Jaylin', 'Yundt', 'https://lorempixel.com/640/480/?71358', 12, 5, 199, 1234, 137, 38, 35, 3, 4, 'Garrick Mayer', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(13, 6, 'elmira', 'Elmira', 'Zemlak', 'https://lorempixel.com/640/480/?91594', 14, 1, 67, 19046, 137, 15, 23, 4, 3, 'Miss Eldridge Barrows DDS', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(14, 3, 'alysson', 'Alysson', 'Osinski', 'https://lorempixel.com/640/480/?83471', 8, 3, 186, 46306, 162, 15, 41, 1, 5, 'Jazmyne Farrell IV', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(15, 2, 'dalton', 'Dalton', 'Little', 'https://lorempixel.com/640/480/?46677', 1, 2, 187, 40977, 53, 21, 24, 1, 6, 'Osvaldo Cremin', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(16, 2, 'elfrieda', 'Elfrieda', 'Stracke', 'https://lorempixel.com/640/480/?87448', 11, 5, 181, 1401, 137, 47, 39, 6, 6, 'Lora Dicki', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(17, 6, 'garland', 'Garland', 'Wiza', 'https://lorempixel.com/640/480/?26057', 5, 3, 135, 17796, 172, 16, 45, 1, 1, 'Ms. Retha Muller', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(18, 4, 'jermaine', 'Jermaine', 'Roob', 'https://lorempixel.com/640/480/?50466', 19, 6, 290, 7090, 136, 27, 40, 1, 5, 'Mr. Ezra Eichmann DDS', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(19, 6, 'arjun', 'Arjun', 'Hessel', 'https://lorempixel.com/640/480/?63713', 3, 6, 49, 6126, 117, 16, 33, 5, 1, 'Dr. Gloria Olson II', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(20, 4, 'brenden', 'Brenden', 'McCullough', 'https://lorempixel.com/640/480/?13554', 19, 5, 273, 32699, 70, 39, 5, 2, 2, 'Luis Dicki', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(21, 2, 'halie', 'Halie', 'Reilly', 'https://lorempixel.com/640/480/?10202', 11, 2, 158, 2634, 122, 38, 39, 5, 5, 'Prof. Mustafa Donnelly MD', 1, NULL, '2020-03-22 09:47:44', '2020-03-22 09:47:44'),
(22, 5, 'kaitlin', 'Kaitlin', 'Schaden', 'https://lorempixel.com/640/480/?44165', 7, 6, 251, 42425, 155, 22, 48, 4, 5, 'Sherman Little DVM', 1, NULL, '2020-03-22 09:47:45', '2020-03-22 09:47:45'),
(23, 6, 'valentina', 'Valentina', 'Borer', 'https://lorempixel.com/640/480/?35674', 2, 2, 48, 20622, 198, 23, 21, 2, 4, 'Kirstin Johnston', 1, NULL, '2020-03-22 09:47:45', '2020-03-22 09:47:45'),
(24, 3, 'dwight', 'Dwight', 'Zemlak', 'https://lorempixel.com/640/480/?38217', 15, 1, 149, 46315, 171, 24, 15, 5, 5, 'Prof. Noemi Pacocha', 1, NULL, '2020-03-22 09:47:45', '2020-03-22 09:47:45'),
(25, 3, 'ana', 'Ana', 'Hirthe', 'https://lorempixel.com/640/480/?22477', 5, 3, 357, 30064, 65, 13, 38, 5, 6, 'Dr. Berenice Rath', 1, NULL, '2020-03-22 09:47:45', '2020-03-22 09:47:45'),
(26, 3, 'alvera', 'Alvera', 'Murray', 'https://lorempixel.com/640/480/?75128', 18, 5, 328, 37705, 191, 26, 45, 2, 6, 'Raoul Schoen', 1, NULL, '2020-03-22 09:47:45', '2020-03-22 09:47:45'),
(27, 3, 'oral', 'Oral', 'Robel', 'https://lorempixel.com/640/480/?68022', 2, 3, 118, 48125, 89, 31, 3, 3, 2, 'Arlene Ward', 1, NULL, '2020-03-22 09:47:45', '2020-03-22 09:47:45'),
(28, 2, 'edd', 'Edd', 'Macejkovic', 'https://lorempixel.com/640/480/?41486', 16, 2, 188, 37490, 183, 7, 11, 5, 4, 'Dr. Karina Predovic DVM', 1, NULL, '2020-03-22 09:47:45', '2020-03-22 09:47:45'),
(29, 6, 'gage', 'Gage', 'Ortiz', 'https://lorempixel.com/640/480/?90162', 10, 1, 398, 33883, 119, 38, 40, 6, 3, 'Ashley Haag', 1, NULL, '2020-03-22 09:47:45', '2020-03-22 09:47:45'),
(30, 1, 'jamar', 'Jamar', 'Ankunding', 'https://lorempixel.com/640/480/?85556', 8, 4, 308, 49418, 128, 31, 9, 4, 3, 'Breana Lockman', 1, NULL, '2020-03-22 09:47:45', '2020-03-22 09:47:45'),
(31, 6, 'allen', 'Allen', 'Johnston', 'https://lorempixel.com/640/480/?51166', 17, 5, 19, 8334, 152, 16, 14, 5, 1, 'Zoie Gislason', 1, NULL, '2020-03-22 09:47:45', '2020-03-22 09:47:45'),
(32, 2, 'samir', 'Samir', 'Boehm', 'https://lorempixel.com/640/480/?49081', 10, 3, 341, 38713, 101, 30, 41, 2, 2, 'Prof. Soledad Quitzon', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(33, 4, 'elmore', 'Elmore', 'Hermiston', 'https://lorempixel.com/640/480/?50103', 5, 2, 231, 6486, 85, 27, 6, 1, 1, 'Hermann Koepp', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(34, 3, 'myriam', 'Myriam', 'Zieme', 'https://lorempixel.com/640/480/?46250', 14, 1, 160, 24211, 118, 27, 37, 1, 1, 'Dr. Kiera Cole', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(35, 5, 'josue', 'Josue', 'Streich', 'https://lorempixel.com/640/480/?47479', 17, 2, 238, 22769, 156, 17, 1, 1, 3, 'Mrs. Gladyce Keeling Jr.', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(36, 2, 'luz', 'Luz', 'Glover', 'https://lorempixel.com/640/480/?91328', 6, 2, 195, 8727, 153, 35, 40, 5, 6, 'Dr. Mckayla Schmidt Jr.', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(37, 3, 'stephen', 'Stephen', 'Johnson', 'https://lorempixel.com/640/480/?18172', 13, 4, 325, 48194, 91, 15, 26, 6, 2, 'Lorenza Roberts I', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(38, 2, 'anita', 'Anita', 'Berge', 'https://lorempixel.com/640/480/?34774', 11, 1, 398, 3358, 117, 31, 36, 5, 3, 'Angelica Macejkovic I', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(39, 2, 'jacques', 'Jacques', 'Ryan', 'https://lorempixel.com/640/480/?22606', 14, 4, 354, 9316, 78, 34, 2, 4, 6, 'Fidel Wintheiser', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(40, 3, 'darien', 'Darien', 'Huels', 'https://lorempixel.com/640/480/?76506', 2, 5, 172, 28114, 170, 50, 33, 3, 2, 'Adolf Renner', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(41, 5, 'sven', 'Sven', 'Harvey', 'https://lorempixel.com/640/480/?85060', 20, 6, 63, 24878, 124, 28, 2, 2, 4, 'Domenico Hagenes', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(42, 3, 'niko', 'Niko', 'Bergstrom', 'https://lorempixel.com/640/480/?34364', 4, 6, 43, 31319, 174, 33, 26, 6, 1, 'Garnett Moore V', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(43, 6, 'isaac', 'Isaac', 'Schuppe', 'https://lorempixel.com/640/480/?56990', 7, 1, 150, 37276, 174, 14, 47, 3, 3, 'Maida Strosin', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(44, 6, 'myrna', 'Myrna', 'Kutch', 'https://lorempixel.com/640/480/?46316', 14, 4, 362, 48398, 54, 34, 26, 2, 5, 'Mr. Dennis Mayer DDS', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(45, 3, 'sam', 'Sam', 'Krajcik', 'https://lorempixel.com/640/480/?42891', 9, 6, 332, 23452, 114, 19, 3, 2, 2, 'Prof. Amari Leffler DVM', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(46, 1, 'noelia', 'Noelia', 'Kulas', 'https://lorempixel.com/640/480/?30120', 12, 2, 67, 29348, 127, 11, 23, 6, 5, 'Eldon Kuphal', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(47, 4, 'guido', 'Guido', 'Mertz', 'https://lorempixel.com/640/480/?26836', 18, 2, 243, 11155, 109, 46, 11, 6, 5, 'Daphney McClure', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(48, 6, 'vena', 'Vena', 'Klein', 'https://lorempixel.com/640/480/?26502', 9, 1, 329, 23344, 168, 3, 24, 3, 3, 'Mr. Kyle Hyatt', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(49, 6, 'darrick', 'Darrick', 'Wunsch', 'https://lorempixel.com/640/480/?32535', 7, 2, 157, 7824, 151, 2, 7, 3, 6, 'Manuel Jaskolski', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46'),
(50, 3, 'felicia', 'Felicia', 'Mayer', 'https://lorempixel.com/640/480/?76962', 8, 5, 245, 44152, 116, 8, 10, 3, 2, 'Dana Koss', 1, NULL, '2020-03-22 09:47:46', '2020-03-22 09:47:46');

-- --------------------------------------------------------

--
-- Table structure for table `points`
--

CREATE TABLE `points` (
  `id` int(10) UNSIGNED NOT NULL,
  `match_played` int(11) NOT NULL,
  `match_won` int(11) NOT NULL,
  `match_lost` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active,0-inactive',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'soft_deleted_at',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `points`
--

INSERT INTO `points` (`id`, `match_played`, `match_won`, `match_lost`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 100, 50, 50, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `club_state` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year_join` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active,0-inactive',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'soft_deleted_at',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `slug`, `name`, `logo`, `banner`, `club_state`, `year_join`, `description`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'india', 'India', 'https://i.pinimg.com/originals/98/47/41/984741289b5f8c97151b0cf2547b6559.jpg', 'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/IND.png', '', '1926', 'India', 1, NULL, NULL, NULL),
(2, 'west-indes', 'West indes', 'https://live.staticflickr.com/3920/14616740477_c0e45fc9b1_b.jpg', 'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/WI.png', '', '1926', 'West indes', 1, NULL, NULL, NULL),
(3, 'sri-lanka', 'Sri Lanka', 'https://www.brandsvectorlogo.com/content/uploads/images/984_srilanka_cricket.jpg', 'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/SL.png', '', '1981', 'Sri Lanka', 1, NULL, NULL, NULL),
(4, 'south-africa', 'South Africa', 'https://i.pinimg.com/originals/c0/4d/12/c04d12fe1bed85cfcc917e5cf4c07e29.jpg', 'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/SA.png', '', '1991', 'South Africa', 1, NULL, NULL, NULL),
(5, 'australia', 'Australia', 'https://i.pinimg.com/originals/29/bf/ba/29bfbaf576fd4acc9cc352413fa1ab41.jpg', 'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/AUS.png', '', '1926', 'Australia', 1, NULL, NULL, NULL),
(6, 'pakistan', 'Pakistan', 'https://i.pinimg.com/originals/78/ff/c3/78ffc35075e4de9e307d7ec118ba3c28.jpg', 'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/PAK.png', '', '1952', 'Pakistan', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fixtures`
--
ALTER TABLE `fixtures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `players_player_slug_unique` (`player_slug`);

--
-- Indexes for table `points`
--
ALTER TABLE `points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `teams_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fixtures`
--
ALTER TABLE `fixtures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `points`
--
ALTER TABLE `points`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
