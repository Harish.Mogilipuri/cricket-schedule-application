<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "UserController@index")->name('cricket.home');
Route::get('/teams', "UserController@teamsList")->name('cricket.teams');
Route::get('/fixtures', "UserController@fixturesList")->name('cricket.fixtures');
Route::get('/fixtures/{id}', "UserController@fixturesDetails")->name('cricket.fixtures.details');
Route::get('/ranking', "UserController@rankingList")->name('cricket.rankings');
Route::get('/{team?}/players',"UserController@playersList")->name('cricket.players');
Route::get('/points', "UserController@pointsData")->name('cricket.points');
// Route::resource('teams', 'UserController');