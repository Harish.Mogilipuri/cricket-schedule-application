<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name',30);
            $table->string('logo',250);
            $table->string('banner',250);
            $table->string('club_state',20)->nullable();
            $table->string('year_join',4);
            $table->longText('description');
            $table->boolean('status')->default(1)->comment('1-active,0-inactive');
            $table->softDeletes()->comment('soft_deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
