<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('team_id');
            $table->string('player_slug')->unique();
            $table->string('first_name',60);
            $table->string('last_name',60);
            $table->string('profile_pic',250);
            $table->integer('jersey_no');
            $table->integer('country');
            $table->integer('matches');
            $table->integer('runs');
            $table->integer('highest_scores');
            $table->integer('fifties');
            $table->integer('hundreds');
            $table->integer('role');
            $table->integer('ranking');
            $table->longText('description');
            $table->boolean('status')->default(1)->comment('1-active,0-inactive');
            $table->softDeletes()->comment('soft_deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
