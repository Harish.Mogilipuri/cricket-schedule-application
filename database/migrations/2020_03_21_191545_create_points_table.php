<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // $table->integer('rank');
            // $table->integer('team_id');
            $table->integer('match_played');
            $table->integer('match_won');
            $table->integer('match_lost');
            // $table->integer('points');
            // $table->double('rating');
            $table->boolean('status')->default(1)->comment('1-active,0-inactive');
            $table->softDeletes()->comment('soft_deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points');
    }
}
