<?php

use Illuminate\Database\Seeder;
use App\Models\Players;
use App\Models\Teams;
class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Players::class, 50)->create();
    }
}
