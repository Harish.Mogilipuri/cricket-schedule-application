<?php

use Illuminate\Database\Seeder;
use App\Models\Fixtures;
use App\Models\Teams;
use App\Models\Points;
class FixturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
            'team_a' => '1',
            'team_b' =>  '2',
            'date' =>  '2020-03-18 09:00:00',
            'venue' =>  'Eden Park, Auckland, New Zealand',
            'winner' =>  '1',
            ),
            array(
                'team_a' => '1',
                'team_b' =>  '3',
                'date' =>  '2020-03-19 09:00:00',
                'venue' =>  'Wanderers, Windhoek, Namibia',
                'winner' =>  '1',
                ),
                array(
                    'team_a' => '1',
                    'team_b' =>  '6',
                    'date' =>  '2020-03-19 09:00:00',
                    'venue' =>  'Wanderers, Windhoek, Namibia',
                    'winner' =>  '1',
                    ),
                    array(
                        'team_a' => '4',
                        'team_b' =>  '5',
                        'date' =>  '2020-03-19 09:00:00',
                        'venue' =>  'National Stadium, Karachi, Pakistan',
                        'winner' =>  '4',
                        ),
                        array(
                            'team_a' => '1',
                            'team_b' =>  '6',
                            'date' =>  '2020-04-01 09:00:00',
                            'venue' =>  'National Stadium, Karachi, Pakistan',
                            'winner' =>  '0',
                            ),
                            array(
                                'team_a' => '4',
                                'team_b' =>  '5',
                                'date' =>  '2020-04-19 09:00:00',
                                'venue' =>  'National Stadium, Karachi, Pakistan',
                                'winner' =>  '0',
                                ),
                                array(
                                    'team_a' => '3',
                                    'team_b' =>  '5',
                                    'date' =>  '2020-02-19 09:00:00',
                                    'venue' =>  'National Stadium,  Visakhapatnam, Andhra Pradesh',
                                    'winner' =>  '3',
                                    ),
                                    array(
                                        'team_a' => '3',
                                        'team_b' =>  '5',
                                        'date' =>  '2020-02-20 09:00:00',
                                        'venue' =>  'National Stadium, Visakhapatnam, Andhra Pradesh',
                                        'winner' =>  '3',
                                        ),
                                        array(
                                            'team_a' => '2',
                                            'team_b' =>  '4',
                                            'date' =>  '2020-01-20 09:00:00',
                                            'venue' =>  'Cricket Stadium,  Visakhapatnam, Andhra Pradesh',
                                            'winner' =>  '4',
                                            ),
                                            array(
                                                'team_a' => '4',
                                                'team_b' =>  '6',
                                                'date' =>  '2020-01-03 09:00:00',
                                                'venue' =>  ' Cricket Stadium,  Visakhapatnam, Andhra Pradesh',
                                                'winner' =>  '6',
                                                ),
              
    );
    Fixtures::insert($data);
    $dataPoints=array(
        'match_played'=>'100',
        'match_won'=>'50',
        'match_lost'=>'50',
    );
    Points::insert($dataPoints);
    }
}
