<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Teams;
class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
            'slug' => 'india',
            'name' =>  'India',
            'banner' =>  'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/IND.png',
            'logo' =>  'https://i.pinimg.com/originals/98/47/41/984741289b5f8c97151b0cf2547b6559.jpg',
            'club_state' =>  '',
            'year_join' => '1926',
            'description' =>  'India',
            ),
            array(
            'slug' => 'west-indes',
            'name' =>  'West indes',
            'banner' =>  'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/WI.png',
            'logo' =>  'https://live.staticflickr.com/3920/14616740477_c0e45fc9b1_b.jpg',
            'club_state' =>  '',
            'year_join' => '1926',
            'description' =>  'West indes',
             ),
             array(
                'slug' => 'sri-lanka',
                'name' =>  'Sri Lanka',
                'banner' =>  'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/SL.png',
                'logo' =>  'https://www.brandsvectorlogo.com/content/uploads/images/984_srilanka_cricket.jpg',
                'club_state' =>  '',
                'year_join' => '1981',
                'description' =>  'Sri Lanka',
                 ),
             array(
            'slug' => 'south-africa',
            'name' =>  'South Africa',
            'banner' =>  'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/SA.png',
            'logo' =>  'https://i.pinimg.com/originals/c0/4d/12/c04d12fe1bed85cfcc917e5cf4c07e29.jpg',
            'club_state' =>  '',
            'year_join' => '1991',
            'description' =>  'South Africa',
             ), 
             array(
                'slug' => 'australia',
                'name' =>  'Australia',
                'banner' =>  'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/AUS.png',
                'logo' =>  'https://i.pinimg.com/originals/29/bf/ba/29bfbaf576fd4acc9cc352413fa1ab41.jpg',
                'club_state' =>  '',
                'year_join' => '1926',
                'description' =>  'Australia',
                 ), 
                 array(
                    'slug' => 'pakistan',
                    'name' =>  'Pakistan',
                    'banner' =>  'https://resources.pulse.icc-cricket.com/ICC/teams/thumbnail/men/PAK.png',
                    'logo' =>  'https://i.pinimg.com/originals/78/ff/c3/78ffc35075e4de9e307d7ec118ba3c28.jpg',
                    'club_state' =>  '',
                    'year_join' => '1952',
                    'description' =>  'Pakistan',
                     ),  
              
    );
        Teams::insert($data);
    }
}
