<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Players;
use App\Models\Teams;
use Illuminate\Support\Facades\File;
$factory->define(Players::class, function (Faker $faker) {
    $name=$faker->unique()->firstName;
    $slug=strtolower(str_replace('+', ' ', $name));
    $teams = Teams::all()->pluck('id')->toArray();
    return [
        'team_id' => $faker->randomElement($teams),
        'player_slug' => $slug,
        'first_name' => $name,
        'last_name' => $faker->lastName,
        'profile_pic' => $faker->imageUrl($width = 640, $height = 480),
        'jersey_no' => rand(1,20),
        'country' => rand(1,6),
        'matches' => rand(1,400),
        'runs' => rand(1,50000),
        'highest_scores' => rand(50,200),
        'fifties' => rand(1,50),
        'hundreds' => rand(1,50),
        'role' => rand(1,6),
        'ranking' => rand(1,6),
        'description' => $faker->name,
    ];
});
