<?php
namespace App\Repositories\Cricket;

interface CricketInterface{


    public function getAllTeam($slug);
    public function matchFixtures();
    public function fixturesDetails($matchId);
    public function getTeamPoints();
    public function rankingList();
    
}
?>