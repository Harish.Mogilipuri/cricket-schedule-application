<?php 
namespace App\Repositories\Cricket;


use App\Repositories\Cricket\CricketInterface;
use App\Models\Teams;
use App\Models\Players;
use App\Models\Fixtures;
use Illuminate\Support\Facades\DB;
class CricketRepository implements CricketInterface
{
    public $cricket;

    function __construct(Teams $team) {
    $this->team = $team;
    }

    /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used for get all teams with players
    */
    public function getAllTeam($slug)
    {
        return $this->team->where('slug', $slug )->with('players')->first();
    }
    /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used for get all match fixtures list
    */
    public function matchFixtures(){
         return Fixtures::with('teamA')->with('teamB')->orderBy('date','desc')->get();
       }
       /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used for get all match fixtures list with players list
    */
    public function fixturesDetails($matchId){
        return Fixtures::where('id',$matchId)->with('teamA')->with('teamB')->with('teamADetails')->with('teamBDetails')->orderBy('date','desc')->first();
      }
       

       /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used for get all teams points based on matches won list
    */
    public function getTeamPoints(){
       return $pointsList = DB::table('fixtures as f')
        ->selectRaw("t.name as teamName, SUM(CASE WHEN ((f.team_a=t.id or f.team_b=t.id)) THEN 1 ELSE 0 END) AS TotalMatches, SUM(CASE WHEN ((f.team_a=t.id or f.team_b=t.id) and f.winner=t.id) THEN 1 ELSE 0 END) AS WinCount, SUM(CASE WHEN ((f.team_a=t.id or f.team_b=t.id) and f.winner<>t.id and f.winner<>0) THEN 1 ELSE 0 END) AS LostCount, SUM(CASE WHEN ((f.team_a=t.id or f.team_b=t.id) and f.winner=0) THEN 1 ELSE 0 END) AS ResultNotDeclared")
        ->join('teams as t',function($join)
                {
                     $join->on('t.id', '=', 'f.team_a')
                     ->orOn('t.id','=','f.team_b');
                })
        ->whereNull('t.deleted_at')
        ->GroupBy('t.id')
        ->orderBy('WinCount','desc')
        ->get();
      }

      
      /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used for get all  players rankings
    */
    public function rankingList()
    {
        return Players::with('team')->orderBy('runs','desc')->get()->take(10);
    }
}

    ?>