<?php

namespace App\Http\Controllers;

use App\Models\Points;
use Illuminate\Http\Request;
use App\Models\Teams;
use App\Repositories\Cricket\CricketInterface;

class UserController extends Controller
{
    public function __construct(CricketInterface $cricket)
    {
        $this->cricket = $cricket;
    }
    /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used home page
    */
    public function index()
    {
     return view('user.home');
    }
    /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used teams list with loadmore option
    */
    public function teamsList(Request $request)
    {
        try{
            $teams = Teams::simplePaginate(3);
            if ($request->ajax()) {
                $view = view('user.teamsAjax', compact('teams'))->render();
                return response()->json(['html' => $view]);
            } else {
                return view('user.teams', compact('teams'));
            }
        } catch (\Exception $e) {
            httpResponse(array('description' => 'Internal Server Error'), 500);
        }
    }
    /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used for fixtures matches list
    */
    public function fixturesList(Request $request)
    {
        try{
            $fixtures = $this->cricket->matchFixtures();
            return view('user.fixtures', compact('fixtures'));
            } catch (\Exception $e) {
                return view('errors.500');
            }
    }
    /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used for fixtures matches list with players details based on match id
    */
    public function fixturesDetails(Request $request,$id)
    {
        try{
            $matchId=base64_decode($id);
            $fixtures = $this->cricket->fixturesDetails($matchId);
            return view('user.fixtures_details', compact('fixtures'));
            } catch (\Exception $e) {
                return view('errors.500');
            }
    }
    /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used  players details based on slug
    */
    public function playersList(Request $request)
    {
        try{
        $slug = $request->team;
        $teams = $this->cricket->getAllTeam($slug);
        return view('user.players', compact('teams'));
        } catch (\Exception $e) {
            return view('errors.500');
        }
    }
    /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used  get team wise points details based on points table data
    */
    public function pointsData()
    {
        try{
            $pointsList = $this->cricket->getTeamPoints();
            $points=Points::first();
            return view('user.points', compact('pointsList','points'));
            } catch (\Exception $e) {
                return view('errors.500');
            }
    }
    /*
    * Created By Harish Mogilipuri Mar 22 2020
    * Updated By Harish Mogilipuri Mar 22 2020
    @this method used top 10 players ranking list
    */
    public function rankingList()
    {
        try{
        $ranking = $this->cricket->rankingList();
        return view('user.ranking', compact('ranking'));
        } catch (\Exception $e) {
            return view('errors.500');
        }
    }

}
