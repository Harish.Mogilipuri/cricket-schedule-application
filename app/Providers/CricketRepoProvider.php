<?php
namespace App\Providers;

use App\Repositories\Cricket\CricketInterface;
use App\Repositories\Cricket\CricketRepository;
use Illuminate\Support\ServiceProvider;

class CricketRepoProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CricketInterface::class,CricketRepository::class);
    }
}
?>