<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Players extends Model
{
    public function team()
    {
        return $this->hasOne('App\Models\Teams','id', 'team_id');
    }
}
