<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fixtures extends Model
{

     /*
      Function to get match with hasOne team a details.
     * @Created by Harish Mogilipuri Mar 22 2019
     * @updated by Harish Mogilipuri Mar 22 2019
     * @param
     */
    public function teamA()
    {
        return $this->hasOne('App\Models\Teams','id', 'team_a');
    }

    /*
      Function to get match with hasOne team b details.
     * @Created by Harish Mogilipuri Mar 22 2019
     * @updated by Harish Mogilipuri Mar 22 2019
     * @param
     */
    public function teamB()
    {
        return $this->hasOne('App\Models\Teams', 'id', 'team_b');
    }
    /*
      Function to get match with hasOne winner team a details.
     * @Created by Harish Mogilipuri Mar 22 2019
     * @updated by Harish Mogilipuri Mar 22 2019
     * @param
     */
    public function winnerTeam()
    {
        return $this->hasOne('App\Models\Teams', 'id', 'winner');
    }

    /*
      Function to get teamA with hasMany player list with a details.
     * @Created by Harish Mogilipuri Mar 22 2019
     * @updated by Harish Mogilipuri Mar 22 2019
     * @param
     */
    public function teamADetails()
    {
        return $this->hasMany('App\Models\Players','team_id', 'team_a');
    }
    /*
      Function to get teamB with hasMany player list with a details.
     * @Created by Harish Mogilipuri Mar 22 2019
     * @updated by Harish Mogilipuri Mar 22 2019
     * @param
     */
    public function teamBDetails()
    {
        return $this->hasMany('App\Models\Players','team_id', 'team_b');
    }
}
