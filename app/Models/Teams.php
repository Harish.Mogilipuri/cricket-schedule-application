<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Players;
class Teams extends Model
{
    
    public function players()
    {
        return $this->hasMany('App\Models\Players','team_id', 'id');
    }
}
