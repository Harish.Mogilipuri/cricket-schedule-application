<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title') | Cricket</title>
  <meta name="Description" content="Cricket" />
  <meta name="Keyword" Content="Cricket" />
  <meta name="author" content="Cricket" />
  <link rel="shortcut icon" href="https://cdn0.iconfinder.com/data/icons/sports-70/60/Sports-Mintie-46_-_Cricket-512.png" />
  @section('styles')
  @show
  <style>
    .hgh420 {
      height: 420px
    }


    #header1 {
      overflow: hidden;
    }

    .suggestions li {
      color: #000;
      padding: 5px 8px;
      border-bottom: 1px dashed #aaa;
      cursor: pointer;
      text-align: center;
    }

    .sticky {
      position: fixed;
      top: 0;
      width: 100%;
      z-index: 999
    }

    .sticky+.content {
      padding-top: 60px;
    }

    .dropdown-menu>li>a:focus,
    .dropdown-menu>li>a:hover {
      background-color: transparent !important;
    }

    .show-on-hover:hover>ul.dropdown-menu {
      display: block;
    }

    .logins-adjj {
      position: absolute;
      left: 6px;
      color: #333333;
    }
  </style>

  <!-- Bootstrap -->
  <link href="{{asset('user/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet" />
  <link rel="shortcut icon" href="" size="32*32">
  <!--custom css-->
  <link href="{{asset('user/css/custom.css')}}" type="text/css" rel="stylesheet" />
  <!--menu-->
  <link href="{{asset('user/css/menu.css')}}" type="text/css" rel="stylesheet" />
  <!-- Icomoon Icon Fonts-->
  <link href="{{asset('user/css/font-awesome.min.css')}}" rel="stylesheet" />

</head>

<body class="home_bg_fixed">
  <!--header start--->
  <section id="header">
    <div class="container-fluid no-padd sticky" id="header1">
      <div class="col-md-12 header-top">
        <div class="container">
          <div class="col-md-12">
            <div class="col-md-2 col-xs-6 header-top-left">
              <ul class="nav navbar-nav mob-m-0">
                <li class="dropdown">
                  <a href="{{route('cricket.fixtures')}}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;Live&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu" style="background-color:#fff">
                    <li><a href="">Live</a></li>
                    <li class="divider"></li>
                    <li><a href="">Upcoming</a></li>
                  </ul>
                </li>
              </ul>
            </div>
            <div class="col-md-4 no-padd">
              <ul class="header-social hidden-xs hidden-sm">

                <a href="{{route('cricket.fixtures')}}" class="col-wh"> <i class="fa fa-map-marker" aria-hidden="true"></i><b> Live &nbsp; </b></a>
                <a href="{{route('cricket.fixtures')}}" class="col-wh"> <i class="fa fa-map-marker" aria-hidden="true"></i><b> Upcoming &nbsp; </b></a>

              </ul>
            </div>

            <div class="col-md-6 col-xs-6 no-padd header-top-right">
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="clearfix"></div>

      <div class="col-md-12 header-menu">
        <div class="container">
          <div class="col-md-2 col-sm-2 no-pad logo">
            <a href="{{route('cricket.home')}}">
              <img src="https://www.freepnglogos.com/uploads/cricket-logo-png/pitch-blue-corporate-cricket-bash-experience-cricket-9.png" alt="logo" class="img-responsive logo-size" /></a></div>
          <div class="col-md-10 header-menulist">
            <div id='cssmenu' class="col-md-12 no-pad">
              <ul>
                <li class="<?php echo ($__env->yieldContent('title') == 'Ranking') ? 'active' : ''; ?>"><a href="{{route('cricket.rankings')}}">Rankings</a></li>
                <li class="<?php echo ($__env->yieldContent('title') == 'Points') ? 'active' : ''; ?>"><a href="{{route('cricket.points')}}">Points</a></li>
                <li class="<?php echo ($__env->yieldContent('title') == 'Fixtures') ? 'active' : ''; ?>"><a href="{{route('cricket.fixtures')}}">Fixtures</a></li>
                <li class="<?php echo ($__env->yieldContent('title') == 'Teams') ? 'active' : ''; ?>"><a href="{{route('cricket.teams')}}">Teams</a></li>
                <li class="<?php echo ($__env->yieldContent('title') == 'Cricket') ? 'active' : ''; ?>"><a href="{{route('cricket.home')}}">Home</a></li>

              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </section>
  <!--header end--->
  <div class="clearfix"></div>
  <div class="clearfix">&nbsp;</div>

  @yield('content')


  <div class="clearfix"></div>
  <div class="clearfix">&nbsp;</div>
  <!--footer Start-->
  <footer class="footer">
    <div class="container">
      <div class="col-md-12 footer-main">
        <div class="clearfix">&nbsp;</div>
        <div class="col-md-4 no-padd">
          <div class="col-md-12 footer-main-left">
            <h2>About Us</h2>
            <p>Find out more about the International Cricket Council; the Organisation and our Members, our Partners, our Match officials, the sport of cricket.</p>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="col-md-4">
          <div class="col-md-12 footer-main-left">
            <h2 class="text-center">Useful Links</h2>
            <ul class="col-md-5">
              <li><a href="{{route('cricket.home')}}">Home</a></li>
              <li><a href="{{route('cricket.teams')}}">Teams</a></li>

            </ul>
            <ul class="col-md-7">
              <li><a href="{{route('cricket.rankings')}}">Ranks</a></li>
              <li><a href="{{route('cricket.points')}}">Points</a></li>

            </ul>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="col-md-12 footer-main-left">
            <h2>Contact Us</h2>
            <ul>

              <p>Mobile&nbsp;:&nbsp;+91 000 000000 | 0000 000000</p>
              <p>Email:test@test.com</p>
            </ul>
          </div>
        </div>

      </div>

      <div class="col-md-12 subfooter">
        <div class="container">
          <div class="col-md-12 subfooter-main">
            <ul class="list-inline">
              <li class="col-md-6 text-left">2020 All &copy;&nbsp;Reserved&nbsp;@cricket.com</li>
              <li class="col-md-6 text-right">Designed &amp; Developed By :<a href="" target="_blank">cricket</a></li>
            </ul>
          </div>
        </div>
      </div>
  </footer>
  <!--footer end-->
  <script src="{{asset('user/js/jquery.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap.min.js')}}"></script>
  @section('script')
  @show
</body>

</html>