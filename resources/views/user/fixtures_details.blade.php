@extends('layouts.master')
@section('title', 'Fixtures')
@section('styles')
@parent
<style>
section{
    float:left;
    width:100%;
    background: #ecdbfb;  /* fallback for old browsers */
    padding:30px 0;
}
.card{
    border-radius: 4px;
    background: #fff;
    box-shadow: 0 6px 10px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
      transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),.3s box-shadow,.3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);
  padding: 14px 80px 18px 36px;
  cursor: pointer;
 
}

.card:hover{
     transform: scale(1.05);
  box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
}

.card h3{
  font-weight: 600;
}

.card .img1{
  position: absolute;
  top: 20px;
  right: 235px;
  max-height: 120px;
  width:100px;height:100px;
}
.card .img2{
  position: absolute;
  top: 20px;
  right: 15px;
  max-height: 120px;
  width:100px;height:100px;
}
.card .vs{
    position: absolute;
    top: 51px;
    right: 76px;
    max-height: 120px;
    width: 100px;
    height: 100px;
    font-size: 30px;
}

@media(max-width: 990px){
  .card{
    margin: 20px;
  }
} 
</style>
@endsection
@section('content')
<section>
  
<div class="container">
  <div class="row">
  
  @if($fixtures)
    <div class="col-md-12 cricket-fix">
      <div class="card card-1">
        
        <div>
        <h3>{{$fixtures->teamA->name}}  V  {{$fixtures->teamB->name}} <span style="color:green;"> <?= (($fixtures->winner==0)?'':'( '.$fixtures->winnerTeam->name.' Won The Match'.')');?></span></h3>
        <p><?= date("l jS \of F Y h:i A", strtotime($fixtures->date));?> | {{$fixtures->venue}}</p>
         </div>
         <div >
        <img class="img1"  src="{{$fixtures->teamA->logo}}"/> <span class="vs">V</span> <img class="img2"  src="{{$fixtures->teamB->logo}}"/>
         </div>
      </div>

       

    </div>
    <div class="clearfix">&nbsp;</div>
    
                @else
                <div class="col-md-12 text-center">
                    <p>No Matches found</p>
                </div>
                @endif


                <div class="container">
	<div class="row">
    <div class="col-md-6">
              <div class="panel panel-default user_panel">
            <div class="panel-heading">
                <h3 class="panel-title"><b>{{$fixtures->teamA->name}} Squad information</b></h3>
            </div>
            <div class="panel-body">
				<div class="table-container">
                    <table class="table-users table" border="0">
                        <tbody>
                        @if(count($fixtures->teamADetails)>0)
                        @foreach($fixtures->teamADetails as $player)
                            <tr>
                                <td width="10">
                                    <img class="pull-left img-circle nav-user-photo" width="50" src="{{$player->profile_pic}}" onerror="if (this.src != 'https://www.shareicon.net/data/512x512/2016/06/30/788957_people_512x512.png') this.src = 'https://www.shareicon.net/data/512x512/2016/06/30/788957_people_512x512.png';" />  
                                </td>
                                <td>
                                {{$player->first_name}} {{$player->last_name}}
                                </td>
                                <td>
                                Jersey No: {{$player->jersey_no}} 
                                </td>
                                
                            </tr>
                            @endforeach
                            @else
                            <tr><td colspan="3">Squad information not available</td></tr>
                            @endif
                            
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        </div>


        <div class="col-md-6">
              <div class="panel panel-default user_panel">
            <div class="panel-heading">
                <h3 class="panel-title"><b>{{$fixtures->teamB->name}} Squad information</b></h3>
            </div>
            <div class="panel-body">
				<div class="table-container">
                    <table class="table-users table" border="0">
                        <tbody>
                        @if(count($fixtures->teamBDetails)>0)
                        @foreach($fixtures->teamBDetails as $player)
                            <tr>
                                <td width="10">
                                    <img class="pull-left img-circle nav-user-photo" width="50" src="{{$player->profile_pic}}" onerror="if (this.src != 'https://www.shareicon.net/data/512x512/2016/06/30/788957_people_512x512.png') this.src = 'https://www.shareicon.net/data/512x512/2016/06/30/788957_people_512x512.png';" />  
                                </td>
                                <td>
                                {{$player->first_name}} {{$player->last_name}}
                                </td>
                                <td>
                                Jersey No: {{$player->jersey_no}} 
                                </td>
                                
                            </tr>
                            @endforeach
                            @else
                            <tr><td colspan="3">Squad information not available</td></tr>
                            @endif
                            
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        </div>



	</div>
</div>
</section>
@endsection