@extends('layouts.master')

@section('title', 'Teams')

@section('content')
<div class="clearfix"></div>
<div class="clearfix">&nbsp;</div>
<section>

    <div class="container">

        <div class="row">

            @if(count($teams)>0)
            <div id="post-data">
                @include('user.teamsAjax')
            </div>
            @else
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>No Teams Found</p>
                </div>
            </div>
            @endif
            <div class="ajax-load text-center" style="display:none">
                <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More Teams</p>
            </div>
            @if(count($teams)>0)
            <div class="text-center " id="remove-row">
            <div class="clearfix"></div>
            <div class="clearfix">&nbsp;</div>
                <button style="background: #a270d2;color: #fff;font-weight: 600;" id="btn-more" data-id="1" type="button" class="btn"> Load More &nbsp; </button>
            </div>
            @endif
            <input type="hidden" id="page" value="1" />
        </div>

    </div>

</section>
@endsection


@section('script')
@parent
<script type="text/javascript">
    var page = $('#page').val();

    function searchData() {
        $('#remove-row').show();
        $("#btn-more").html("Load More");
        $('#page').val(1);
        $('#post-data').html('');
        loadMoreData(0);
        page++;
    }
    $(document).ready(function() {
        $(document).on('click', '#btn-more', function() {
            var page = $('#page').val();
            $("#btn-more").html("Loading....");
            page++;
            $('#page').val(page);
            loadMoreData(page);
        });
    });

    function loadMoreData(page) {
        var keyword = $('#search').val();
        var spId = $('#spId').val();
        
        $.ajax({
                url: '?page=' + page,
                type: "get",
                beforeSend: function() {
                    $('.ajax-load').show();
                }
            })
            .done(function(data) {
                $("#btn-more").html("Load More");
                if (data.html == "") {
                    $('#btn-more').html("No more offers found");
                    $('.ajax-load').hide();
                    // return;
                }
                $('.ajax-load').hide();
                $("#post-data").append(data.html);
            })
            .fail(function(jqXHR, ajaxOptions, thrownError) {
                $('#btn-more').html("server not responding...");
            });
            //scroll to next click
        $('html, body').animate({
        scrollTop: $("#btn-more").offset().top
         }, 1000);
         //scroll to next click
    }
</script>

@endsection