@extends('layouts.master')

@section('title', 'Points')

@section('content')
<section style="background:#fff;">
  
    <div class="container">
      <h1>TEAM RANKINGS</h1>
    <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Rank</th>
      <th scope="col">Team</th>
      <th scope="col">Matches Played</th>
      <th scope="col">Matches Won</th>
      <th scope="col">Matches Lost</th>
      <th scope="col">Result Not Declared</th>
      <th scope="col">Points</th>
      <th scope="col">Rating</th>
    </tr>
  </thead>
  <tbody>
    <?php 
     $match_played_points= $points->match_played;
    $match_won_points= $points->match_won;
    $match_lost_points= $points->match_lost;
    ?>
  @if(count($pointsList)>0)
  <?php $i=0; ?>
   @foreach($pointsList as $matchPoints)
   <?php $i++; 
    $plus=$matchPoints->TotalMatches*$match_played_points;
    $won_plus=$matchPoints->WinCount*$match_won_points;
    $sub=$matchPoints->LostCount*$match_won_points;
   ?>
    <tr>
      <th scope="row"><?= $i ?></th>
      <td>{{$matchPoints->teamName}}</td>
      <td>{{$matchPoints->TotalMatches}}</td>
      <td>{{$matchPoints->WinCount}}</td>
      <td>{{$matchPoints->LostCount}}</td>
      <td>{{$matchPoints->ResultNotDeclared}}</td>
      <td>{{$total_points=$plus+$won_plus-$sub}}</td>
      <td><?php $rating = $total_points/$matchPoints->TotalMatches; 
     echo  round($rating, 2);
      ?></td>
    </tr>
    @endforeach
    @else
    <tr><td colspan="7">No Records Found</td></tr>
  @endif

    
  </tbody>
</table>
    </div>
  
</section>
@endsection