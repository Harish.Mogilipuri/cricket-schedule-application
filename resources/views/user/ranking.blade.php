@extends('layouts.master')

@section('title', 'Ranking')
@section('content')

<section class="new-deal" style="background: #fff;">


    <div class="container">
        <div class="col-sm-12">

            <div class="col-sm-12">
<h1> Top 10 Ranking</h1>
                @if(count($ranking)>0)
                <?php $i=0;?>
                @foreach($ranking as $player)
                <?php $i++;?>
                <div class="col-sm-3">
                    <div class="item-slide">
                        <div class="box-img">
                            <a href="dfd"><img src="{{$player->profile_pic}}" onerror="if (this.src != 'https://www.shareicon.net/data/512x512/2016/06/30/788957_people_512x512.png') this.src = 'https://www.shareicon.net/data/512x512/2016/06/30/788957_people_512x512.png';" class="img-responsive" style="width:100%;"></a>
                            <div class="text-wrap">
                                <h4><b> &nbsp; <a href="">{{$player->first_name}} {{$player->last_name}} </a></b></h4>
                                <p> &nbsp;{{$player->runs}} Runs </p>
                                <p> &nbsp;{{$i}} Rank </p>
                            </div>
                        </div>
                        <div class="slide-hover">
                            <div class="text-wrap">

                                <h4><b> <span class="deal-data"> <a href="">Details</a></span></b></h4>
                                <div class="desc">
                                    <h3>Highest Scores: {{$player->highest_scores}} </h3>

                                </div>
                                <div class="book-now-c">
                                    <a href="javascript:void(0);" onclick="playerDetails('{{$player->first_name}}','{{$player->last_name}}','{{$player->matches}}','{{$player->runs}}','{{$player->highest_scores}}','{{$player->fifties}}','{{$player->hundreds}}','{{$i}}')">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-md-12 text-center">
                    <p>No Players found</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>



<!-- Modal -->
<div style="z-index: 99999999999;" class="modal fade" id="palyerDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pname" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="pname"></h4>
                        <table class="table">

                            <tbody>
                                <tr>
                                    <td><b>Total Matches</b></b></td>
                                    <td>:</td>
                                    <td id="matches"></td>
                                </tr>
                                <tr>
                                    <td><b>Total Runs</b></td>
                                    <td>:</td>
                                    <td id="runs"></td>
                                </tr>
                                <tr>
                                    <td><b>Highest Scores</b></td>
                                    <td>:</td>
                                    <td id="highest_scores"></td>
                                </tr>
                                <tr>
                                    <td><b>Fifties</b></td>
                                    <td>:</td>
                                    <td id="fifties"></td>
                                </tr>
                                <tr>
                                    <td><b>Hundreds</b></td>
                                    <td>:</td>
                                    <td id="hundreds"></td>
                                </tr>
                                <tr>
                                    <td><b>Ranking</b></td>
                                    <td>:</td>
                                    <td id="ranking"></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
@parent
<script>
    function playerDetails(first_name, last_name, matches, runs, highest_scores, fifties, hundreds, ranking) {
        $('#palyerDetails').modal('show');
        $('#matches,.pname,#runs,#highest_scores,#fifties,#hundreds,#ranking').html('');
        $('.pname').html(first_name + ' ' + last_name);
        $('#matches').html(matches);
        $('#runs').html(runs);
        $('#highest_scores').html(highest_scores);
        $('#fifties').html(fifties);
        $('#hundreds').html(hundreds);
        $('#ranking').html(ranking);
    }
</script>
@endsection