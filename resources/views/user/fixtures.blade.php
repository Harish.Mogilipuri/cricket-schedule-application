@extends('layouts.master')
@section('title', 'Fixtures')
@section('styles')
@parent
<style>
section{
    float:left;
    width:100%;
    background: #ecdbfb;  /* fallback for old browsers */
    padding:30px 0;
}
.card{
    border-radius: 4px;
    background: #fff;
    box-shadow: 0 6px 10px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
      transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),.3s box-shadow,.3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);
  padding: 14px 80px 18px 36px;
  cursor: pointer;
 
}

.card:hover{
     transform: scale(1.05);
  box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
}

.card h3{
  font-weight: 600;
}

.card .img1{
  position: absolute;
  top: 20px;
  right: 235px;
  max-height: 120px;
  width:100px;height:100px;
}
.card .img2{
  position: absolute;
  top: 20px;
  right: 15px;
  max-height: 120px;
  width:100px;height:100px;
}
.card .vs{
    position: absolute;
    top: 51px;
    right: 76px;
    max-height: 120px;
    width: 100px;
    height: 100px;
    font-size: 30px;
}

@media(max-width: 990px){
  .card{
    margin: 20px;
  }
} 
</style>
@endsection
@section('content')
<section>
  
<div class="container">
  <div class="row">

  
  @if(count($fixtures)>0)
   @foreach($fixtures as $match)
    <div class="col-md-12 cricket-fix">
      <a style="text-decoration: none;" href="{{ route('cricket.fixtures.details', ['id' => base64_encode($match->id)]) }}">
      <div class="card card-1">
        
        <div >
        <h3>{{$match->teamA->name}}  V  {{$match->teamB->name}} <span style="color:green;"> <?= (($match->winner==0)?'':'( '.$match->winnerTeam->name.' Won The Match'.')');?></span></h3>
        <p><?= date("l jS \of F Y h:i A", strtotime($match->date));?> | {{$match->venue}}</p>
         </div>
         <div >
        <img class="img1"  src="{{$match->teamA->logo}}"/> <span class="vs">V</span> <img class="img2"  src="{{$match->teamB->logo}}"/>
         </div>
      </div>
     </a>
       

    </div>
    <div class="clearfix">&nbsp;</div>
    @endforeach
                @else
                <div class="col-md-12 text-center">
                    <p>No Matches found</p>
                </div>
                @endif

</section>
@endsection