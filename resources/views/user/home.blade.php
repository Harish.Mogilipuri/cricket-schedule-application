@extends('layouts.master')

@section('title', 'Cricket')

@section('content')
<section class="banner">

<div class="container-fluid no-padd">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner carousel-inner2">
      <div class="item active">
        <img src="https://wallpapersite.com/images/wallpapers/team-india-3135x1332-national-cricket-team-indian-cricket-team-ms-8561.jpg" alt="img1" style="width:100%;">
      </div>

      <div class="item">
        <img src="https://ansunibaate.com/wp-content/uploads/2019/04/bg-cricket-image.jpg" alt="img2" style="width:100%;">
      </div>

      <div class="item">
        <img src="https://crickettimes.com/wp-content/uploads/2019/10/IPL.jpg" alt="img3" style="width:100%;">
      </div>
    </div>


    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

</section>


<section>
    <div class="container-fluid">
      <div class="container">
        <div class="col-sm-12 bg-wh shadow no-padd">


          <div class="col-sm-4 hobby_head">

            <div class="col-sm-11 col-sm-offset-1">
              <h3 class="">What is Cricket?</h3>
              <h4>Want to learn more about cricket? Watch our series of 10 videos to learn everything you need to know to understand this global game.</h4>
            </div>
            <div class="col-sm-offset-3 col-sm-9">

            </div>
          </div>
          <div class="col-sm-8 no-padd hidden-xs hidden-sm">
            <img src="https://www.journalducameroun.com/en/wp-content/uploads/2019/02/15502512655551.jpg" class="img-responsive">
          </div>
          <a href="{{route('cricket.teams')}}"><button type="button" class="btn explore_two btn-lg"> Explore more &nbsp; <i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>
        </div>
      </div>
    </div>
  </section>

  <div class="clearfix">&nbsp;</div>
  <div class="clearfix">&nbsp;</div>
  <div class="clearfix">&nbsp;</div>
  <section>
    <div class="container-fluid">
      <div class="container">
        <div class="col-sm-12 bg-wh shadow no-padd">
          <div class="col-sm-8 no-padd">
            <img src="https://resources.pulse.icc-cricket.com/ICC/photo/2017/01/30/44780aa2-2b6d-4048-897c-6606762b3d38/GettyImages-463485384_Cropped.jpg" class="img-responsive">
          </div>
          <div class="col-sm-4 abacus_head">
            <h3> Match Officials </h3>
            <div class="col-sm-11">
              <h4> Learn more about the referees, elite umpires and their coaches whose job it is to make the final, all important decisions.</h4>

            </div>
            <div class="col-sm-offset-3 col-sm-9">
              <a href="{{route('cricket.fixtures')}}"><button type="button" class="btn explore btn-lg"> Explore more &nbsp; <i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div class="clearfix">&nbsp;</div>
  <div class="clearfix">&nbsp;</div>

  <div class="clearfix">&nbsp;</div>
  <section>
    <div class="container-fluid">
      <div class="container">
        <div class="col-sm-12 bg-wh shadow no-padd">


          <div class="col-sm-4 hobby_head">

            <div class="col-sm-11 col-sm-offset-1">
              <h3 class="">About</h3>
              <h4>Find out more about the International Cricket Council; the Organisation and our Members, our Partners, our Match officials, the sport of cricket.</h4>
            </div>
            <div class="col-sm-offset-3 col-sm-9">

            </div>
          </div>
          <div class="col-sm-8 no-padd hidden-xs hidden-sm">
            <img src="https://static.telegraphindia.com/derivative/THE_TELEGRAPH/1684104/16X9/imagedd3447ca-847e-499b-834b-78995fa74811.jpg" class="img-responsive">
          </div>
          <a href="{{route('cricket.rankings')}}"><button type="button" class="btn explore_two btn-lg"> Explore more &nbsp; <i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>
        </div>
      </div>
    </div>
  </section>
  <div class="clearfix">&nbsp;</div>

  <div class="clearfix">&nbsp;</div>

  
<!--section banner end-->
<div class="clearfix"></div>
<div class="clearfix">&nbsp;</div>

@endsection