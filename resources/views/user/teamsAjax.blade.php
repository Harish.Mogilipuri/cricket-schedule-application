@if(count($teams)>0)
@foreach($teams as $team)
<div class="col-md-4">
<a style="text-decoration: none;"  href="{{ route('cricket.players', ['team' => $team->slug]) }}">
    		    <div class="card profile-card-3">
    		        <div class="background-block">
    		            <img src="{{$team->banner}}" alt="profile-sample1" class="background"/>
    		        </div>
    		        <div class="profile-thumb-block">
    		            <img src="{{$team->logo}}" alt="profile-image" class="profile"/>
    		        </div>
    		        <div class="card-content">
                    <h2>{{$team->name}}</small></h3>
                    <div class="icon-block">
                        <a style="text-decoration: none;" href="{{ route('cricket.players', ['team' => $team->slug]) }}"><i class="fa fa-eye"></i></a>
                       </div>
                    </div>
                </div>
</a>
               </div>
@endforeach
@else
<div class="col-md-12 text-center">
<p>No More Teams found</p>
</div>
<script>
    $('#remove-row').hide();
</script>
@endif